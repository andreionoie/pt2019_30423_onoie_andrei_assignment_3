DROP DATABASE IF EXISTS `schooldb`;
CREATE DATABASE `schooldb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `schooldb`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `idclient` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idclient`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `idproduct` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `cost` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  PRIMARY KEY (`idproduct`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `orderbill`;
CREATE TABLE `orderbill` (
  `idorder` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) DEFAULT NULL,
  `productid` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`idorder`)
  /*KEY `productid_idx` (`productid`),
  KEY `clientid_idx` (`clientid`)
  CONSTRAINT `clientid` FOREIGN KEY (`clientid`) REFERENCES `client` (`idclient`),
  CONSTRAINT `productid` FOREIGN KEY (`productid`) REFERENCES `product` (`idproduct`)*/
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

insert into client (name, address, email) values ('Aigneis Pembridge', '48840 Stone Corner Way', 'apembridge0@ft.com');
insert into client (name, address, email) values ('Erwin Verbruggen', '40 Straubel Avenue', 'everbruggen1@pinterest.com');
insert into client (name, address, email) values ('Pierce Logsdale', '609 North Junction', 'plogsdale2@blinklist.com');
insert into client (name, address, email) values ('Inglis de Vaen', '82280 Scott Alley', 'ide3@homestead.com');
insert into client (name, address, email) values ('Odie Goodale', '54 Anniversary Terrace', 'ogoodale4@parallels.com');
insert into client (name, address, email) values ('Sarette Cristofaro', '0165 Village Green Trail', 'scristofaro5@nationalgeographic.com');
insert into client (name, address, email) values ('Filmore Dominik', '5 Milwaukee Avenue', 'fdominik6@icio.us');
insert into client (name, address, email) values ('Filip Sowray', '59 Canary Drive', 'fsowray7@so-net.ne.jp');
insert into client (name, address, email) values ('Aidan Record', '7368 Westport Plaza', 'arecord8@dmoz.org');
insert into client (name, address, email) values ('Lenna Osgordby', '934 Schmedeman Way', 'losgordby9@deliciousdays.com');
insert into client (name, address, email) values ('Wes Caton', '9432 Eastlawn Street', 'wcatona@cyberchimps.com');
insert into client (name, address, email) values ('Prent Reddings', '0760 6th Road', 'preddingsb@hud.gov');
insert into client (name, address, email) values ('Kettie Pottinger', '5951 Bayside Plaza', 'kpottingerc@surveymonkey.com');
insert into client (name, address, email) values ('Sollie Elger', '05654 Hoepker Hill', 'selgerd@usa.gov');
insert into client (name, address, email) values ('Fax Sappson', '23947 Meadow Ridge Trail', 'fsappsone@utexas.edu');
insert into client (name, address, email) values ('Amandie Bleackly', '71904 Rieder Crossing', 'ableacklyf@de.vu');
insert into client (name, address, email) values ('Ian Edger', '843 Riverside Pass', 'iedgerg@goo.gl');
insert into client (name, address, email) values ('Polly Dorset', '7254 Melby Circle', 'pdorseth@webmd.com');
insert into client (name, address, email) values ('Liuka Hugnet', '25905 Hanson Terrace', 'lhugneti@sina.com.cn');
insert into client (name, address, email) values ('Charyl Flasby', '18 Tennessee Point', 'cflasbyj@github.com');

insert into product (name, cost, stock) values ('Hand Towel', 6, 585);
insert into product (name, cost, stock) values ('Yogurt - Banana, 175 Gr', 10, 103);
insert into product (name, cost, stock) values ('Mushroom - Lg - Cello', 20, 108);
insert into product (name, cost, stock) values ('Tuna - Fresh', 10, 778);
insert into product (name, cost, stock) values ('Sauce - Vodka Blush', 6, 801);
insert into product (name, cost, stock) values ('V8 Pet', 18, 965);
insert into product (name, cost, stock) values ('Sugar - Individual Portions', 3, 531);
insert into product (name, cost, stock) values ('Glass - Juice Clear 5oz 55005', 11, 784);
insert into product (name, cost, stock) values ('Appetizer - Chicken Satay', 10, 449);
insert into product (name, cost, stock) values ('Beef - Salted', 17, 472);
insert into product (name, cost, stock) values ('Ecolab - Mikroklene 4/4 L', 6, 527);
insert into product (name, cost, stock) values ('Pork - Sausage Casing', 11, 159);
insert into product (name, cost, stock) values ('Tomatoes - Grape', 20, 330);
insert into product (name, cost, stock) values ('Beans - Navy, Dry', 7, 114);
insert into product (name, cost, stock) values ('Calvados - Boulard', 12, 34);
insert into product (name, cost, stock) values ('Potatoes - Yukon Gold 5 Oz', 19, 125);
insert into product (name, cost, stock) values ('Soup - French Onion', 18, 466);
insert into product (name, cost, stock) values ('Tart - Raisin And Pecan', 21, 162);
insert into product (name, cost, stock) values ('Pie Shell - 5', 11, 949);
insert into product (name, cost, stock) values ('Appetizer - Spring Roll, Veg', 5, 543);
insert into product (name, cost, stock) values ('Veal - Insides Provini', 17, 920);
insert into product (name, cost, stock) values ('Spinach - Spinach Leaf', 5, 143);
insert into product (name, cost, stock) values ('Apple - Granny Smith', 12, 32);
insert into product (name, cost, stock) values ('Champagne - Brights, Dry', 2, 230);
insert into product (name, cost, stock) values ('Bagel - Plain', 21, 922);
insert into product (name, cost, stock) values ('Pastry - Banana Muffin - Mini', 21, 877);
insert into product (name, cost, stock) values ('Gherkin - Sour', 16, 35);
insert into product (name, cost, stock) values ('Ginger - Pickled', 8, 667);
insert into product (name, cost, stock) values ('Appetizer - Mushroom Tart', 22, 8);
insert into product (name, cost, stock) values ('Raspberries - Fresh', 23, 166);
insert into product (name, cost, stock) values ('Versatainer Nc - 888', 14, 786);
insert into product (name, cost, stock) values ('Lamb Tenderloin Nz Fr', 16, 406);
insert into product (name, cost, stock) values ('Sterno - Chafing Dish Fuel', 15, 459);
insert into product (name, cost, stock) values ('Chicken - Leg, Boneless', 3, 676);
insert into product (name, cost, stock) values ('Chicken - Base, Ultimate', 10, 910);
insert into product (name, cost, stock) values ('Tea - Jasmin Green', 14, 887);
insert into product (name, cost, stock) values ('V8 - Tropical Blend', 18, 735);
insert into product (name, cost, stock) values ('Beer - Moosehead', 14, 196);
insert into product (name, cost, stock) values ('Instant Coffee', 12, 383);
insert into product (name, cost, stock) values ('Wine - Rioja Campo Viejo', 10, 734);
