package bll;

import bll.validators.ClientValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ClientBLL {
    private List<Validator<Client>> validators;
    private ClientDAO clientDAO;

    public ClientBLL() {
        validators = new ArrayList<Validator<Client>>();
        validators.add(new ClientValidator());
        clientDAO = new ClientDAO();
    }

    public Client findClientById(int id) {
        Client cl = clientDAO.findById(id);
        if (cl == null) {
            throw new NoSuchElementException("The client with id =" + id + " was not found!");
        }
        return cl;
    }

    public Client insertClient(Client client) throws Exception {
        for (Validator<Client> v : validators) {
            v.validate(client);
        }
        return clientDAO.insert(client);
    }

    public void delete(int id) {
        clientDAO.delete(id);
    }

    public Client updateClient(Client client) {
        return clientDAO.update(client);
    }

    public List<Object> findAll() {
        return new ArrayList<Object>(clientDAO.findAll());
    }

}
