package bll.validators;

import model.Product;

public class ProductValidator implements Validator<Product> {

    public void validate(Product product) {
        if (product.getCost() < 0 || product.getStock() < 0)
            throw new IllegalArgumentException("Negative cost or stock values.");
    }
}
