package bll.validators;

import model.Client;

public class ClientValidator implements Validator<Client> {
    public void validate(Client client) {
        if (!client.getEmail().matches("[^@]+@[^\\.]+\\..+"))
            throw new IllegalArgumentException("Bad email format.");
    }
}
