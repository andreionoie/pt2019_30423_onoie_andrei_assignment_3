package bll.validators;

import model.OrderBill;

public class OrderValidator implements Validator<OrderBill> {
    public void validate(OrderBill orderBill) {
        if (orderBill.getAmount() < 0 || orderBill.getClientid() < 0 || orderBill.getProductid() < 0)
            throw new IllegalArgumentException("Negative amount, or bad id's.");
    }
}
