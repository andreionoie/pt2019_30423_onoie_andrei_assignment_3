package bll;

import bll.validators.ProductValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ProductBLL {
    private List<Validator<Product>> validators;
    private ProductDAO productDAO;

    public ProductBLL() {
        validators = new ArrayList<Validator<Product>>();
        validators.add(new ProductValidator());
        productDAO = new ProductDAO();
    }

    public Product findProductById(int id) {
        Product cl = productDAO.findById(id);
        if (cl == null) {
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return cl;
    }

    public Product insertProduct(Product product) {
        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        return productDAO.insert(product);
    }

    public void delete(int id) {
        productDAO.delete(id);
    }

    public Product updateProduct(Product product) {
        return productDAO.update(product);
    }

    public List<Object> findAll() {
        return new ArrayList<Object>(productDAO.findAll());
    }

    public int getProductStock(int id) {
        Product p = findProductById(id);

        return p.getStock();
    }
}
