package bll;

import bll.validators.OrderValidator;
import bll.validators.Validator;
import dao.OrderDAO;
import model.OrderBill;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class OrderBLL {
    private List<Validator<OrderBill>> validators;
    private OrderDAO orderDAO;

    public OrderBLL() {
        validators = new ArrayList<Validator<OrderBill>>();
        validators.add(new OrderValidator());
        orderDAO = new OrderDAO();
    }

    public OrderBill findOrderById(int id) {
        OrderBill or = orderDAO.findById(id);
        if (or == null) {
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        return or;
    }
    public OrderBill insertOrder(OrderBill orderBill) {
        for (Validator<OrderBill> v : validators) {
            v.validate(orderBill);
        }
        return orderDAO.insert(orderBill);
    }

    public void delete(int id) {
        orderDAO.delete(id);
    }

    public OrderBill updateOrder(OrderBill orderBill) {
        return orderDAO.update(orderBill);
    }

    public List<Object> findAll() {
        return new ArrayList<Object>(orderDAO.findAll());
    }

}
