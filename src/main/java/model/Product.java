package model;

public class Product {
    private int idproduct;
    private String name;
    private int cost;
    private int stock;

    public Product() {
    }

    public Product(int idproduct, String name, int cost, int stock) {
        this.idproduct = idproduct;
        this.name = name;
        this.cost = cost;
        this.stock = stock;
    }

    public int getIdproduct() {
        return idproduct;
    }

    public void setIdproduct(int idproduct) {
        this.idproduct = idproduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
