package model;

public class OrderBill {
    private int idorder;
    private int clientid;
    private int productid;
    private int amount;

    public OrderBill() {
    }

    public OrderBill(int idorder, int clientid, int productid, int amount) {
        this.idorder = idorder;
        this.clientid = clientid;
        this.productid = productid;
        this.amount = amount;
    }

    public int getIdorder() {
        return idorder;
    }

    public void setIdorder(int idorder) {
        this.idorder = idorder;
    }

    public int getClientid() {
        return clientid;
    }

    public void setClientid(int clientid) {
        this.clientid = clientid;
    }

    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
