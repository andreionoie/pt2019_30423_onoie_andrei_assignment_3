package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuController {
    private MenuView menuView;

    public MenuController() {
        menuView = new MenuView();

        menuView.addClientsListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menuView.dispose();
                new ClientController();
            }
        });

        menuView.addProductsListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menuView.dispose();
                new ProductController();
            }
        });

        menuView.addOrdersListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menuView.dispose();
                new OrderController();
            }
        });
    }

    public static void main(String[] args) {
        new MenuController();
    }
}
