package presentation;

import bll.ClientBLL;
import dao.ClientDAO;
import model.Client;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ClientController {
    private ClientView clientView;

    public ClientController() {
        clientView = new ClientView();
        ClientBLL clientBLL = new ClientBLL();
        clientView.drawTable(clientBLL.findAll());
        setTableListener();

        clientView.setBackButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clientView.dispose();
                new MenuController();
            }
        });

        clientView.setAddButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Client c = new Client(-1,
                        clientView.getNameField(),
                        clientView.getAddrField(),
                        clientView.getEmailField());

                try {
                    clientBLL.insertClient(c);
                    clientView.drawTable(clientBLL.findAll());
                    setTableListener();
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(clientView, "Invalid input data.");
                }
            }
        });

        clientView.setDeleteButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clientBLL.delete((int) clientView.getTableValueAt(clientView.getSelectedRow(), 0));
                clientView.drawTable(clientBLL.findAll());
                setTableListener();
            }
        });

        clientView.setUpdateButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Client c = new Client(Integer.parseInt(clientView.getIdField()),
                                        clientView.getNameField(),
                                        clientView.getAddrField(),
                                        clientView.getEmailField());

                clientBLL.updateClient(c);
                clientView.drawTable(clientBLL.findAll());
                setTableListener();
            }
        });


    }

    private void setTableListener() {
        clientView.setTableListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                clientView.setIdField(clientView.getTableValueAt(clientView.getSelectedRow(), 0).toString());
                clientView.setNameField(clientView.getTableValueAt(clientView.getSelectedRow(), 1).toString());
                clientView.setAddrField(clientView.getTableValueAt(clientView.getSelectedRow(), 2).toString());
                clientView.setEmailField(clientView.getTableValueAt(clientView.getSelectedRow(), 3).toString());
            }
        });
    }
}
