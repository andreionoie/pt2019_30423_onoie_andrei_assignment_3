package presentation;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class TableHelper {
    public static JTable getPopulatedTable(List<Object> objects) {
        Class objClass = objects.get(0).getClass();

        //DefaultTableModel tm = new DefaultTableModel();

        List<String> headers = new ArrayList<String>();
        for (Field f : objClass.getDeclaredFields()) {
            headers.add(f.getName());
        }

        //tm.setColumnIdentifiers(headers.toArray());
        TableModel tm = new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return objects.size();
            }

            @Override
            public int getColumnCount() {
                return objClass.getDeclaredFields().length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                try {
                    Field f = objClass.getDeclaredFields()[columnIndex];
                    f.setAccessible(true);
                    Object out = f.get(objects.get(rowIndex));
                    f.setAccessible(false);
                    return out;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public String getColumnName(int i) {
                return headers.get(i);
            }
        };
        JTable t = new JTable();

//        for (Object o : objects) {
//            Object[] row = new Object[objClass.getDeclaredFields().length];
//
//            int i=0;
//            for (Field f : objClass.getDeclaredFields()) {
//                f.setAccessible(true);
//                try {
//                    row[i] = f.get(o);
//                    i++;
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                    return null;
//                }
//            }
//
//            System.out.println("Added row: " + row);
//            tm.addRow(row);
//        }

        t.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        t.setModel(tm);
        resizeColumnWidth(t);

        return t;
    }

    private static void resizeColumnWidth(JTable table) {
        final TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            int width = 15; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width +1 , width);
            }
            if(width > 300)
                width=300;
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }
}
