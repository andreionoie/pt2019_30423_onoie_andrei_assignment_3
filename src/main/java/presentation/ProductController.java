package presentation;

import bll.ProductBLL;
import dao.ProductDAO;
import model.Product;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ProductController {
    private ProductView productView;

    public ProductController() {
        productView = new ProductView();
        ProductBLL productBLL = new ProductBLL();
        productView.drawTable(productBLL.findAll());
        setTableListener();

        productView.setBackButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                productView.dispose();
                new MenuController();
            }
        });

        productView.setAddButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Product p = new Product(-1,
                            productView.getNameField(),
                            Integer.parseInt(productView.getCostField()),
                            Integer.parseInt(productView.getStockField()));

                    productBLL.insertProduct(p);
                    productView.drawTable(productBLL.findAll());
                    setTableListener();
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(productView, "Invalid values.");
                }
            }
        });
        productView.setDeleteButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                productBLL.delete((int) productView.getTableValueAt(productView.getSelectedRow(), 0));
                productView.drawTable(productBLL.findAll());
                setTableListener();
            }
        });

        productView.setUpdateButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Product p = new Product(Integer.parseInt(productView.getIdField()),
                        productView.getNameField(),
                        Integer.parseInt(productView.getCostField()),
                        Integer.parseInt(productView.getStockField()));

                productBLL.updateProduct(p);
                productView.drawTable(productBLL.findAll());
                setTableListener();
            }
        });

    }

    private void setTableListener() {
        productView.setTableListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                productView.setIdField(productView.getTableValueAt(productView.getSelectedRow(), 0).toString());
                productView.setNameField(productView.getTableValueAt(productView.getSelectedRow(), 1).toString());
                productView.setCostField(productView.getTableValueAt(productView.getSelectedRow(), 2).toString());
                productView.setStockField(productView.getTableValueAt(productView.getSelectedRow(), 3).toString());
            }
        });
    }
}
