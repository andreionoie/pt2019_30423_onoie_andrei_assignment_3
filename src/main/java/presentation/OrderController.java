package presentation;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.OrderBill;
import model.Product;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class OrderController {
    private OrderView orderView;

    public OrderController() {
        orderView = new OrderView();
        ClientBLL clientBLL = new ClientBLL();
        ProductBLL productBLL = new ProductBLL();
        OrderBLL orderBLL = new OrderBLL();

        orderView.drawTables(clientBLL.findAll(), productBLL.findAll());

        orderView.setBackButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderView.dispose();
                new MenuController();
            }
        });
        
        orderView.setOrderButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (orderView.getSelProductID() < 0 || orderView.getSelClientID() < 0) {
                    JOptionPane.showMessageDialog(orderView, "Please select client and product.");
                    return;
                }
                if (orderView.getAmount() > productBLL.getProductStock(orderView.getSelProductID())) {
                    JOptionPane.showMessageDialog(orderView, "Insufficient stock for product.");
                    return;
                }

                OrderBill o = orderBLL.insertOrder(new OrderBill(2,
                                                        orderView.getSelClientID(),
                                                        orderView.getSelProductID(),
                                                        orderView.getAmount()));
                Product p = productBLL.findProductById(orderView.getSelProductID());
                p.setStock(p.getStock() - orderView.getAmount());
                productBLL.updateProduct(p);

                System.out.println("Placed order #" + o.getIdorder());
                writeOrderToFile(o, clientBLL.findClientById(o.getClientid()), p);
                orderView.drawTables(clientBLL.findAll(), productBLL.findAll());
            }
        });
    }

    private void writeOrderToFile(OrderBill o, Client c, Product p) {
        String filename = "order_" + o.getIdorder() + ".txt";
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
            bw.write("OrderBill #" + o.getIdorder() + "\n");
            bw.write("Client " + c.getName() + ", address " + c.getAddress() + ", email: " + c.getEmail() + "\n");
            bw.write("Total cost: $" + o.getAmount() * p.getCost() + "\n");
            bw.write("Content: " + p.getName() + " x " + o.getAmount() + "\n");
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
