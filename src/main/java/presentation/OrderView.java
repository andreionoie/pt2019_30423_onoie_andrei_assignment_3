package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

public class OrderView extends JFrame {
    private int WIDTH=900, HEIGHT=600;
    private JSpinner amountSpinner;
    private JButton backButton, orderButton;
    private JTable clientsTable, productsTable;
    private JPanel tablesPanel;

    public OrderView() {
        addComponents();

        setTitle("OrderBill Management - Place OrderBill");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void addComponents() {
        JPanel controlPanel = new JPanel(new GridLayout(1, 6));
        tablesPanel = new JPanel(new GridLayout(1, 2));

        amountSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 9999, 1));
        orderButton = new JButton("Order");
        backButton = new JButton("Back");

        controlPanel.add(new JLabel("Amount:"));
        controlPanel.add(amountSpinner);
        controlPanel.add(new JLabel());
        controlPanel.add(orderButton);
        controlPanel.add(new JLabel());
        controlPanel.add(backButton);


        add(tablesPanel, BorderLayout.CENTER);
        add(controlPanel, BorderLayout.SOUTH);
    }

    public void drawTables(List<Object> clients, List<Object> products) {
        tablesPanel.removeAll();

        productsTable = TableHelper.getPopulatedTable(products);
        productsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        productsTable.setDefaultEditor(Object.class, null);
        productsTable.setVisible(true);

        JScrollPane sp1 = new JScrollPane(productsTable);
        sp1.setVisible(true);
        sp1.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        clientsTable = TableHelper.getPopulatedTable(clients);
        clientsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        clientsTable.setDefaultEditor(Object.class, null);
        clientsTable.setVisible(true);

        JScrollPane sp2 = new JScrollPane(clientsTable);
        sp2.setVisible(true);
        sp2.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        tablesPanel.add(sp2);
        tablesPanel.add(sp1);

        tablesPanel.revalidate();
        tablesPanel.repaint();
        tablesPanel.updateUI();
    }

    public void setOrderButtonListener(ActionListener al) {
        orderButton.addActionListener(al);
    }

    public void setBackButtonListener(ActionListener al) {
        backButton.addActionListener(al);
    }

    public int getAmount() {
        return (int) amountSpinner.getValue();
    }

    public int getSelClientID() {
        if (clientsTable.getSelectedRow() < 0)
            return -1;

        return (int) clientsTable.getValueAt(clientsTable.getSelectedRow(), 0);
    }

    public int getSelProductID() {
        if (productsTable.getSelectedRow() < 0)
            return -1;

        return (int) productsTable.getValueAt(productsTable.getSelectedRow(), 0);
    }

}
