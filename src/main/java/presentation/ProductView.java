package presentation;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

public class ProductView extends JFrame {
    private int WIDTH=800, HEIGHT=500;
    private JButton addButton, updateButton, deleteButton;
    private JButton backButton;
    private JTextField idField, nameField, costField, stockField;
    private JPanel tablesPanel;
    private JTable productsTable;

    public ProductView() {
        addComponents();

        setTitle("OrderBill Management - Manage Products");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void addComponents() {
        JPanel pane1, pane2, pane3, pane4, pane5, inputPane;
        pane1 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        pane2 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        pane3 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        pane4 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        pane5 = new JPanel();

        inputPane = new JPanel(new GridLayout(8, 1));
        tablesPanel = new JPanel();

        final String blankText = "                                                   ";
        idField = new JTextField(blankText,20);
        idField.setText("");
        nameField = new JTextField(blankText, 20);
        nameField.setText("");
        costField = new JTextField(blankText, 20);
        costField.setText("");
        stockField = new JTextField(blankText, 20);
        stockField.setText("");

        pane1.add(new JLabel("Product ID:"));
        pane1.add(idField);
        idField.setEditable(false);

        pane2.add(new JLabel("Name:"));
        pane2.add(nameField);

        pane3.add(new JLabel("Cost:"));
        pane3.add(costField);

        pane4.add(new JLabel("Stock:"));
        pane4.add(stockField);

        addButton = new JButton("Add");
        updateButton = new JButton("Update");
        deleteButton = new JButton("Remove");
        backButton = new JButton("Back");

        pane5.add(addButton);
        pane5.add(updateButton);
        pane5.add(deleteButton);

        inputPane.add(pane1);
        inputPane.add(pane2);
        inputPane.add(pane3);
        inputPane.add(pane4);
        inputPane.add(new JLabel());
        inputPane.add(pane5);
        inputPane.add(new JLabel());
        inputPane.add(backButton);

        add(inputPane, BorderLayout.WEST);
        add(tablesPanel, BorderLayout.CENTER);
    }

    public void drawTable(List<Object> objects) {
        tablesPanel.removeAll();

        productsTable = TableHelper.getPopulatedTable(objects);
        productsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        productsTable.setDefaultEditor(Object.class, null);
        productsTable.setVisible(true);

        JScrollPane sp2 = new JScrollPane(productsTable);
        sp2.setVisible(true);
        sp2.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        tablesPanel.add(sp2);

        tablesPanel.revalidate();
        tablesPanel.repaint();
        tablesPanel.updateUI();
    }

    public void setAddButtonListener(ActionListener al) {
        addButton.addActionListener(al);
    }

    public void setUpdateButtonListener(ActionListener al) {
        updateButton.addActionListener(al);
    }

    public void setDeleteButtonListener(ActionListener al) {
        deleteButton.addActionListener(al);
    }

    public void setBackButtonListener(ActionListener al) {
        backButton.addActionListener(al);
    }

    public void setTableListener(ListSelectionListener lsl) {
        productsTable.getSelectionModel().addListSelectionListener(lsl);
    }

    public String getIdField() {
        return idField.getText();
    }

    public void setIdField(String txt) {
        idField.setText(txt);
    }

    public String getNameField() {
        return nameField.getText();
    }

    public void setNameField(String txt) {
        nameField.setText(txt);
    }

    public String getCostField() {
        return costField.getText();
    }

    public void setCostField(String txt) {
        costField.setText(txt);
    }

    public String getStockField() {
        return stockField.getText();
    }

    public void setStockField(String txt) {
        stockField.setText(txt);
    }

    public Object getTableValueAt(int i, int j) {
        return productsTable.getValueAt(i, j);
    }

    public int getSelectedRow() {
        return productsTable.getSelectedRow();
    }
}
