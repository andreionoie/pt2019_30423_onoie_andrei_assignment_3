package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class MenuView extends JFrame {
    private int WIDTH=400, HEIGHT=150;
    private JButton clients, products, orders;

    public MenuView() {
        addComponents();

        setTitle("OrderBill Management - Menu");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void addComponents() {
        JPanel panel = new JPanel(new GridLayout(1, 3));
        clients = new JButton("Clients");
        products = new JButton("Products");
        orders = new JButton("Orders");

        panel.add(clients);
        panel.add(products);
        panel.add(orders);

        add(new JLabel("Select the part you want to manage:", JLabel.CENTER), BorderLayout.NORTH);
        add(panel, BorderLayout.CENTER);
    }

    public void addClientsListener(ActionListener al) {
        clients.addActionListener(al);
    }

    public void addProductsListener(ActionListener al) {
        products.addActionListener(al);
    }

    public void addOrdersListener(ActionListener al) {
        orders.addActionListener(al);
    }
}
