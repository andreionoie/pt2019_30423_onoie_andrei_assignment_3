package presentation;

import model.Client;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ClientView extends JFrame {
    private int WIDTH=800, HEIGHT=500;
    private JButton addButton, updateButton, deleteButton;
    private JButton backButton;
    private JTextField idField, nameField, addrField, emailField;
    private JPanel tablesPanel;
    private JTable clientsTable;

    public ClientView() {
        addComponents();

        setTitle("OrderBill Management - Manage Clients");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void addComponents() {
        JPanel pane1, pane2, pane3, pane4, pane5, inputPane;
        pane1 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        pane2 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        pane3 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        pane4 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        pane5 = new JPanel();

        inputPane = new JPanel(new GridLayout(8, 1));
        tablesPanel = new JPanel();

        final String blankText = "                                                   ";
        idField = new JTextField(blankText, 20);
        idField.setText("");
        nameField = new JTextField(blankText, 20);
        nameField.setText("");
        addrField = new JTextField(blankText, 20);
        addrField.setText("");
        emailField = new JTextField(blankText, 20);
        emailField.setText("");

        pane1.add(new JLabel("Client ID:"));
        pane1.add(idField);
        idField.setEditable(false);

        pane2.add(new JLabel("Name:"));
        pane2.add(nameField);
        pane3.add(new JLabel("Address:"));
        pane3.add(addrField);
        pane4.add(new JLabel("Email:"));
        pane4.add(emailField);

        addButton = new JButton("Add");
        updateButton = new JButton("Update");
        deleteButton = new JButton("Remove");
        backButton = new JButton("Back");

        pane5.add(addButton);
        pane5.add(updateButton);
        pane5.add(deleteButton);

        inputPane.add(pane1);
        inputPane.add(pane2);
        inputPane.add(pane3);
        inputPane.add(pane4);
        inputPane.add(new JLabel());
        inputPane.add(pane5);
        inputPane.add(new JLabel());
        inputPane.add(backButton);

        add(inputPane, BorderLayout.WEST);
        add(tablesPanel, BorderLayout.CENTER);
    }

    public void drawTable(List<Object> objects) {
        tablesPanel.removeAll();

        clientsTable = TableHelper.getPopulatedTable(objects);
        clientsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        clientsTable.setDefaultEditor(Object.class, null);
        clientsTable.setVisible(true);

        JScrollPane sp2 = new JScrollPane(clientsTable);
        sp2.setVisible(true);
        sp2.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        tablesPanel.add(sp2);

        tablesPanel.revalidate();
        tablesPanel.repaint();
        tablesPanel.updateUI();
    }

    public void setAddButtonListener(ActionListener al) {
        addButton.addActionListener(al);
    }

    public void setUpdateButtonListener(ActionListener al) {
        updateButton.addActionListener(al);
    }

    public void setDeleteButtonListener(ActionListener al) {
        deleteButton.addActionListener(al);
    }

    public void setBackButtonListener(ActionListener al) {
        backButton.addActionListener(al);
    }

    public void setTableListener(ListSelectionListener lsl) {
        clientsTable.getSelectionModel().addListSelectionListener(lsl);
    }

    public String getIdField() {
        return idField.getText();
    }

    public void setIdField(String txt) {
        idField.setText(txt);
    }

    public String getNameField() {
        return nameField.getText();
    }

    public void setNameField(String txt) {
        nameField.setText(txt);
    }

    public String getAddrField() {
        return addrField.getText();
    }

    public void setAddrField(String txt) {
        addrField.setText(txt);
    }

    public String getEmailField() {
        return emailField.getText();
    }

    public void setEmailField(String txt) {
        emailField.setText(txt);
    }

    public Object getTableValueAt(int i, int j) {
        return clientsTable.getValueAt(i, j);
    }

    public int getSelectedRow() {
        return clientsTable.getSelectedRow();
    }
}
