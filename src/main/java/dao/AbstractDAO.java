package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 * @Source http://www.java-blog.com/mapping-javaobjects-database-reflection-generics
 */
public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }

    private String getDeclaredFieldsCommaSeparated() {
        List<String> fields = new ArrayList<String>();

        for (Field f : type.getDeclaredFields()) {
            f.setAccessible(true);

            if (f.getName().equals(type.getDeclaredFields()[0].getName()))
                continue;

            fields.add(f.getName());
            f.setAccessible(false);
        }

        return String.join(", ", fields);
    }

    private String getDeclaredFieldsUpdateFormat() {
        StringBuilder sb = new StringBuilder();
        List<String> fields = new ArrayList<String>();

        for (Field f : type.getDeclaredFields()) {
            f.setAccessible(true);

            if (f.getName().equals(type.getDeclaredFields()[0].getName()))
                continue;

                fields.add(" " + f.getName() + " = ? ");
            f.setAccessible(false);
        }

        return String.join(",", fields);
    }

    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " = ?");
        return sb.toString();
    }

    private String createInsertQuery() {
        StringBuilder sb = new StringBuilder();
        List<String> questionMarks = new ArrayList<String>();
        for (int i=0; i < type.getDeclaredFields().length - 1; i++)
            questionMarks.add("?");

        sb.append("INSERT INTO ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append(" ( " + getDeclaredFieldsCommaSeparated() + " )");
        sb.append(" VALUES ");
        sb.append(" ( " + String.join(", ", questionMarks) + " ) ");
        return sb.toString();
    }

    private String createUpdateQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName());
        sb.append(" SET ");
        sb.append(getDeclaredFieldsUpdateFormat());
        type.getDeclaredFields()[0].setAccessible(true);
        sb.append(" WHERE " + type.getDeclaredFields()[0].getName() + " = ?");
        type.getDeclaredFields()[0].setAccessible(false);

        return sb.toString();
    }

    public List<T> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement("SELECT * FROM " + type.getSimpleName());
            resultSet = statement.executeQuery();

            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    public T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        type.getDeclaredFields()[0].setAccessible(true);
        String query = createSelectQuery(type.getDeclaredFields()[0].getName());
        type.getDeclaredFields()[0].setAccessible(false);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            System.out.println(statement);
            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();

        try {
            while (resultSet.next()) {
                T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    field.setAccessible(true);
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);

                    field.setAccessible(false);
                }
                list.add(instance);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }

    public T insert(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createInsertQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            int i=1;
            for (Field f : type.getDeclaredFields()) {
                if (f.getName().equals(type.getDeclaredFields()[0].getName()))
                    continue;

                f.setAccessible(true);
                statement.setObject(i, f.get(t));
                i++;
                f.setAccessible(false);
            }

            System.out.println(statement);
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            Field id = type.getDeclaredFields()[0];
            id.setAccessible(true);
            if (resultSet.next()) {
                resultSet.first();
                id.set(t, resultSet.getInt(1));
            }

        } catch (SQLException | IllegalAccessException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return t;
    }

    public T update(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createUpdateQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int i=1;
            for (Field f : type.getDeclaredFields()) {
                f.setAccessible(true);
                if (f.getName().equals(type.getDeclaredFields()[0].getName()))
                    continue;

                statement.setObject(i, f.get(t));
                i++;

                f.setAccessible(false);
            }
            // set where id = ?
            Field id = type.getDeclaredFields()[0];
            id.setAccessible(true);
            statement.setObject(i, id.get(t));
            id.setAccessible(false);

            System.out.println(statement);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return t;
    }

    public void delete(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        StringBuilder sb = new StringBuilder("DELETE FROM " + type.getSimpleName() + " WHERE ( ");
        type.getDeclaredFields()[0].setAccessible(true);
        sb.append(type.getDeclaredFields()[0].getName() + " = ? )");
        type.getDeclaredFields()[0].setAccessible(false);

        String query = sb.toString();

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            System.out.println(statement);
            // set where id = ?
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

    }
}
